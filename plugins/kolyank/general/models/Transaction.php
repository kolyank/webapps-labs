<?php namespace Kolyank\General\Models;

use Model;

/**
 * Transaction Model
 */
class Transaction extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kolyank_general_transactions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
