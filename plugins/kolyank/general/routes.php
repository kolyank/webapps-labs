<?php

Route::any('/themes/landing/assets/javascript/lang/es.js', function () {
    return redirect('/');
});

Route::group([ 'middleware' => 'web' ], function () {
    // login
    Route::post('/user/signin', 'RainLab\User\Components\Account@onSignin');
    Route::get('/user/logout', 'RainLab\User\Components\Session@onLogout');

    Route::group([ 'middleware' => 'Rainlab\User\Classes\AuthMiddleware' ], function () {
        Route::get('/user/get', 'Kolyank\General\Components\User@onGetUser');
    });
});

