<?php namespace Kolyank\General;

use Backend;
use Kolyank\General\Components\Header;
use Kolyank\General\Components\Home;
use Kolyank\General\Components\About;
use Kolyank\General\Components\Contacts;
use Kolyank\General\Components\User;
use System\Classes\PluginBase;

/**
 * General Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'General',
            'description' => 'No description provided yet...',
            'author'      => 'Kolyank',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents() {
        return [
            Header::class => 'header',
            Home::class => 'home',
            About::class => 'about',
            Contacts::class => 'contacts',
            User::class => 'user'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolyank.general.some_permission' => [
                'tab' => 'General',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'general' => [
                'label'       => 'General',
                'url'         => Backend::url('kolyank/general/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolyank.general.*'],
                'order'       => 500,
            ],
        ];
    }
}
