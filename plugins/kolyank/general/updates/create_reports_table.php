<?php namespace Kolyank\General\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateReportsTable extends Migration
{
    public function up()
    {
        Schema::create('kolyank_general_reports', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolyank_general_reports');
    }
}
