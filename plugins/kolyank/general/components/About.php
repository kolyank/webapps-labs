<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;

class About extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'About Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
