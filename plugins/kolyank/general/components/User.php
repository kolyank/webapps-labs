<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;
use RainLab\User\Facades\Auth;

class User extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'User Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {
        $this->prepareVars();
    }

    public function prepareVars() {
        $this->page['user'] = Auth::getUser();
    }

    public function onGetUser() {
        return response()->json([
            'user' => Auth::getUser()
        ]);
    }
}
