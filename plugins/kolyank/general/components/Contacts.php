<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;

class Contacts extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contacts Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onFakeResponse() {
        return response()->json([
            'success' => true
        ]);
    }
}
