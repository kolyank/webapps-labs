<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;

class Home extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Home Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
