<?php namespace Kolyank\General\Components;

use Cms\Classes\ComponentBase;

class Header extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Header Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
