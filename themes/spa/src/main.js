import RootComponent from './root';
import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import routes from './routes';
import 'vue-awesome/icons/flag';
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
import VueTranslate from 'vue-translate-plugin';
import en from 'locales/en';

axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name=token]').getAttribute('content');

function run() {
    Vue.use(VueTranslate);
    Vue.use(VueRouter);
    Vue.component('icon', Icon);

    const router = new VueRouter({
        base: __dirname,
        mode: 'history',
        routes,
    });

    new Vue({
        render: h => h(RootComponent),
        el: '#app',
        router,
        store,
    });

    Vue.locales({
        en: en,
    });
}

