import LoginComponent from './pages/login/login';
import CompaniesComponent from './pages/companies/companies';
import TransactionsComponent from './pages/transactions/transactions';
import ReportsComponent from './pages/reports/reports';

export default [
    {
        name: 'login',
        path: '/login',
        component: LoginComponent,
    },
    {
        name: 'reports',
        path: '/',
        component: ReportsComponent,
    },
    {
        name: 'companies',
        path: '/companies',
        component: CompaniesComponent,
    },
    {
        name: 'transactions',
        path: '/transactions',
        component: TransactionsComponent,
    },
];
