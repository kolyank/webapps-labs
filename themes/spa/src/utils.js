export const forbidden = 401;

export const getIdFromURL = () => {
    const url = window.location.pathname;
    return Number(url.substring(url.lastIndexOf('/') + 1));
};

// TODO:
export const showMessage = (message, success = true) => {
    alert(message);
};

export const confirmation = () => {
    const message = 'Are you sure?';
    return confirm(message);
};

export const capitalize = (_string) => {
    return _string.replace(/(^|\s)\S/g, l => l.toUpperCase());
};

export const getFileBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.onload = () => resolve(reader.result);
        reader.onerror = (error) => reject(error);

        reader.readAsDataURL(file);
    });
};

export const getImageTypes = () => {
    return [
        'image/jpeg',
        'image/gif',
        'image/png',
        'image/bmp',
        'image/svg+xml',
    ];
};

export const downloadFile = (data, name) => {
    var element = document.createElement('a');
    element.href = data;
    element.setAttribute('download', name);

    var event = document.createEvent('MouseEvents');
    event.initEvent('click', false, true);
    element.dispatchEvent(event);

    return false;
};

export const checkOnline = () => {
    return navigator.onLine;
};

export const getCatFromUrl = () => {
    const url = new URL(window.location.href);
    return url.searchParams.get('cat');
};