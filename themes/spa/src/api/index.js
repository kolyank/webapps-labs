import axios from 'axios';

const config = {
    headers: { 'Content-Type': 'multipart/form-data' },
};

export const login = (login, password) => {
    let formData  = new FormData();
    formData.append('login', login);
    formData.append('password', password);
    return axios.post('/user/signin', formData, config);
};

export const logout = () => {
    return axios.get('/user/logout');
};

export const switchLocale = locale => {
    return axios.post('/locale/set', {locale});
};

export const getUser = () => {
    return axios.get('/user/get');
};

export const getProjects = () => {
    return axios.get('/get/projects');
};

export const getProject = categoryId => {
    return axios.get(`/get/project/${categoryId}`);
};

export const getSubCategories = () => {
    return axios.get('/get/categories');
};

export const getSubCategory = subCategoryId => {
    return axios.get(`/get/category/${subCategoryId}`);
};

export const getFile = categoryId => {
    return axios.get('/category/files/get', { params: { id: categoryId } });
};

export const addFile = (formData, config) => {
    return axios.post('/category/files/add', formData, config);
};

export const deleteFile = (id, categoryId, folderId) => {
    return axios.delete(`/category/files/${id}/${categoryId}/${folderId}`);
};

export const addFolder = (name, categoryId, folderId) => {
    return axios.post('/category/folders/new', {
        name,
        category_id: categoryId,
        folder_id: folderId,
    });
};

export const changeFolder = (categoryId, id) => {
    return axios.post('/category/folders/change', {
        category_id: categoryId,
        folder_id: id,
    });
};

export const deleteFolder = (categoryId, id) => {
    return axios.delete(`/category/folders/${categoryId}/${id}`);
};

export const addSubcategory = (parentId, name) => {
    return axios.post('/subcategory/new', {
        parent_id: parentId,
        subcat_child_name: name,
    });
};

export const getReports = (mode, filtersData, page = 1, count = 10) => {
    return axios.get('/reports/get', {
        params: {
            mode,
            page,
            count,
            filters_data: filtersData,
        },
    });
};

export const getColumns = (type) => {
    return axios.get('/reports/columns/get', {
        params: {
            type: type,
        },
    });
};

export const setColumns = (type, columns) => {
    return axios.post('/reports/columns/set', {
        type: type,
        columns: columns,
    });
};

export const setColumnsDefault = (type) => {
    return axios.post('/reports/columns/set/default', {
        type: type,
    });
};

export const getReportsFilters = (filters) => {
    return axios.get('/reports/filters/get', {
        params: {
            filters,
        },
    });
};

export const getCategories = (projects) => {
    return axios.get('/reports/filters/categories/get', {
        params: {
            projects,
        },
    });
};

export const getSubcategories = (projects, categories) => {
    return axios.get('/reports/filters/subcategories/get', {
        params: {
            projects,
            categories,
        },
    });
};

export const getReportsSettings = () => {
    return axios.get('/reports/settings/get');
};

export const getEmails = () => {
    return axios.get('/documents/emails/get');
};

export const sendEmail = (id, documentId, selectedEmails, typeCode) => {
    return axios.post('/mail/send', {
        id: id,
        document_id: documentId,
        email: selectedEmails.join(', '),
        type: typeCode,
    });
};

export const getDocuments = subCategoryId => {
    return axios.get('/documents/get', {
        params: { id: subCategoryId },
    });
};

export const updateQuestion = (payload, subCategoryId) => {
    let data = {
        state: payload.state,
        text: payload.text,
        question_id: payload.questionId,
        files: payload.files,
        user: payload.user,
    };
    if (payload.offline) {
        data = {...data, ...{offline: payload.offline}};
    }
    return axios.post(`/subcategory/question/update?scId=${subCategoryId}`, data);
};

export const deleteComment = (id, questionId, offline, subCategoryId) => {
    const params = offline ?
        {parent_id: offline.self_parent_id, subcat_child_name: offline.template_name, scId: subCategoryId} :
        {scId: subCategoryId};
    return axios.delete(`/subcategory/question/comment/${id}/${questionId}`, {params});
};

export const generatePdf = subcategoryId => {
    return axios.post('/subcategory/pdf/generate', {
        id: subcategoryId,
    });
};

export const getThumbnail = (file, categoryId, fileId) => {
    let formData  = new FormData();
    fileId ? formData.append('id', fileId) : formData.append('file', file);
    formData.append('site_id', categoryId);
    return axios.post('/file/thumbnail', formData, config);
};

export const getFileChunks = (id, angle = 0) => {
    return axios.post('/file/upload', {
        id: id,
        angle: angle,
    });
};

export const uploadPdf = (file, categoryId) => {
    let formData  = new FormData();
    formData.append('pdf', file);
    formData.append('site_id', categoryId);
    return axios.post('/pdf/upload', formData, config);
};

export const updateFormName = (id, name) => {
    return axios.post('/subcategory/update/form_name', {
        id: id,
        form_name: name,
    });
};

export const generatePDF = (data) => {
    return axios.post('/generate/pdf', data);
};

export const generateExcel = (data) => {
    return axios.post('/generate/excel', data);
};
