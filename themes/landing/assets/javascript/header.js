$(document).ready(function () {
    $(function() {
        $('#menu').smartmenus();
        $('#main-menu').smartmenus('itemActivate', $('a#myItem'));
    });
});

$(document).on('click', '#logout', function () {
    $.request('onLogout', function () {
        success: () => {
            window.location = '/'
        }
    })
});
